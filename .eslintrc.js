module.exports = {
    extends: [
        'eslint-config-bhdouglass',
    ],
    env: {
        node: true,
        mocha: true,
    },
    rules: {},
    overrides: [
        {
            files: "*-test.js",
            rules: {
                'no-unused-expressions': 'off',
            },
        },
    ],
};
