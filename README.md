# Example Node.js Api for Testing and Linting

This is an example Node.js api used for a tutorial about testing and linting.

[Read the full blog post on my blog.](https://blog.bhdouglass.com/nodejs/tutorial/2019/06/24/testing-and-linting-a-nodejs-api.html)

## License

Copyright (C) 2019 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
