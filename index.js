const express = require('express');
const bodyParser = require('body-parser');
const Widget = require('./widget');

const app = express();
app.use(bodyParser.json());


app.get('/', async (req, res) => {
    try {
        let widgets = await Widget.find();
        res.send(widgets);
    }
    catch (err) {
        res.status(500);
        res.send();
    }
});

app.get('/:id', async (req, res) => {
    try {
        let widget = await Widget.findOne({_id: req.params.id});
        res.send(widget);
    }
    catch (err) {
        res.status(500);
        res.send();
    }
});

app.post('/', async (req, res) => {
    let widget = new Widget({
        foo: req.body.foo,
        bar: req.body.bar,
    });

    try {
        await widget.save();
        res.send(widget);
    }
    catch (err) {
        res.status(500);
        res.send();
    }
});

app.put('/:id', async (req, res) => {
    try {
        let widget = await Widget.findOne({_id: req.params.id});
        widget.foo = req.body.foo;
        widget.bar = req.body.bar;

        await widget.save();
        res.send(widget);
    }
    catch (err) {
        res.status(500);
        res.send();
    }
});

app.delete('/:id', async (req, res) => {
    try {
        let widget = await Widget.findOne({_id: req.params.id});
        await widget.remove();

        res.status(204);
        res.send();
    }
    catch (err) {
        res.status(500);
    }
});

app.server = app.listen(8080);
module.exports = app;
