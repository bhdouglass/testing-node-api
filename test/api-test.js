const {expect} = require('chai');
const {factory} = require('factory-girl');
const request = require('supertest');

require('./setup');
const app = require('../index');
const Widget = require('../widget');

describe('API Test', () => {
    context('GET all', () => {
        it('is successful with multiple widgets', async () => {
            await factory.createMany('widget', 5);

            let res = await request(app).get('/').expect(200);

            expect(res.body).to.be.lengthOf(5);
        });

        it('is successful with no widgets', async () => {
            let res = await request(app).get('/').expect(200);

            expect(res.body).to.be.lengthOf(0);
        });

        it('fails with a 500 error', async function() {
            let findStub = this.sinon.stub(Widget, 'find').rejects();

            await request(app).get('/').expect(500);

            expect(findStub).to.have.been.calledOnce;
        });
    });
});
