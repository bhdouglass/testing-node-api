const {factory} = require('factory-girl');

const Widget = require('../../widget');

factory.define('widget', Widget, {
    foo: factory.sequence('Widget.foo', (n) => `foo${n}`),
    bar: factory.sequence('Widget.bar', (n) => `bar${n}`),
});
