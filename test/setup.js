const chai = require('chai');
const FactoryGirl = require('factory-girl');
const mongoose = require('mongoose');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const app = require('../index');

require('./factories/widget');

FactoryGirl.factory.setAdapter(new FactoryGirl.MongooseAdapter());

chai.use(sinonChai);

before(function(done) {
    // Wait for the mongoose to connect
    if (mongoose.connection.readyState != 1) {
        mongoose.connection.once('open', done);
    }

    this.sinon = sinon;
});

afterEach(async function() {
    await FactoryGirl.factory.cleanUp();
    this.sinon.restore();
});

after(() => {
    app.server.close();
    mongoose.connection.close();
});
