const mongoose = require('mongoose');

const mongoUri = process.env.MONGODB_URI || 'mongodb://localhost:27017/test';
mongoose.connect(mongoUri, {useNewUrlParser: true}, (err) => {
    if (err) {
        console.error('database error:', err);
        process.exit(1);
    }
});

const WidgetSchema = mongoose.Schema({
    foo: String,
    bar: String,
});

module.exports = mongoose.model('Widget', WidgetSchema);
